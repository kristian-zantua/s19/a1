//3
const getCube = 2 ** 3

//4
function cubed(result){
	//console.log(result)

	return `The cube of 2 is 8`
}
console.log(cubed(getCube))

//5
const address = {
	street: `258 Washington Ave NW`,
	state: `California`,
	zipcode: `90011 `
}
//6
function describeAddress(address){
	let {street, state, zipcode} = address
	return `I live at ${street}, ${state} ${zipcode}`
}
console.log(describeAddress(address));

//7
const animal = {
	name: `Lolong`,
	kind: `saltwater crocodile`,
	weight: `1075 kgs`,
	length: `20 ft 3 in`
}
//8
function describeAnimal(animal){
	let {name, kind, weight, length} = animal
	return `${name} was a ${kind}. He weighed ${weight} with a measurement of ${length}.`
}
console.log(describeAnimal(animal));

//9
const numbers = [1,2,3,4,5]

//10

numbers.forEach((numbers) => {
	console.log(numbers)
})
//11
let reduceNumber = 0
reduceNumber =  numbers.reduce((num1,num2) => num1 + num2)
console.log(reduceNumber)

//12

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
//13
let dog = new Dog(`Frankie`, 5, `Miniature Daschund`);
console.log(dog);